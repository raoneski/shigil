package me.shigil.entity

import io.ktor.auth.*

data class User(
    val id: Int,
    val nickName: String,
    val phoneNumber: String
) : Principal

data class CreateUser(
    val nickName: String,
    val phoneNumber: String
)

data class UserResponseWithToken(
    val user: User,
    val token: String
)