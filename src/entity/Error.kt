package me.shigil.entity

data class ErrorResponse(
    val message: String,
    val code: Int
)