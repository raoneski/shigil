package me.shigil.user

import com.google.gson.Gson
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.*
import io.ktor.auth.jwt.jwt
import io.ktor.http.HttpStatusCode
import me.shigil.JwtConfig
import me.shigil.entity.CreateUser
import me.shigil.entity.ErrorResponse
import me.shigil.entity.UserResponseWithToken
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import me.shigil.toJson
import me.shigil.user

@Suppress("auth")
fun Routing.auth() {
    val userSource: UserSource = UserSourceImpl()

    /**
     * A public login [Route] used to obtain JWTs
     */
    post("login") {
        val credentials = call.receive<UserPasswordCredential>()
        val user = userSource.findUserByCredentials(credentials)

        val token = JwtConfig.makeToken(user)
        println("login: user=${user.toJson()}, token=$token")
        call.respondText(token)
    }

    post("auth/user") {
        try {
            val user = userSource.createUser(call.receive<CreateUser>())
            val token = JwtConfig.makeToken(user)

            println("auth/user: createUser=${user.toJson()}")
            call.respond(
                UserResponseWithToken(
                    user,
                    token
                )
            )
        } catch (e: Exception) {
            call.respond(
                HttpStatusCode.BadRequest,
                ErrorResponse(
                    message = "No content.",
                    code = 400)
            )
        }
    }

    /**
     * All [Route]s in the authentication block are secured.
     */
    authenticate {
        route("secret") {

            get {
                val user = call.user!!
                call.respond(user)
            }

            put {
                TODO("All your secret routes can follow here")
            }

            get("/users") {
                call.respond(userSource.getAll())
            }
        }


    }

    /**
     * Routes with optional authentication
     */
    authenticate(optional = true) {
        get("optional") {
            val user = call.user
            val response = if (user != null) "authenticated!" else "optional"
            call.respond(response)
        }
    }

}