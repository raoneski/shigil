package me.shigil.user

import io.ktor.auth.UserPasswordCredential
import me.shigil.entity.CreateUser
import me.shigil.entity.User
import me.shigil.testUser

interface UserSource {

    fun findUserById(id: Int): User

    fun findUserByCredentials(credential: UserPasswordCredential): User

    fun createUser(createUser: CreateUser): User

    fun getAll(): List<User>

}

class UserSourceImpl : UserSource {

    override fun findUserById(id: Int): User = users.associateBy(User::id).getValue(id)

    override fun findUserByCredentials(credential: UserPasswordCredential): User = testUser

    override fun createUser(createUser: CreateUser): User {
        val user = User(
            id = users.size,
            nickName = createUser.nickName,
            phoneNumber = createUser.phoneNumber
        )

        users.add(user)

        return user
    }

    override fun getAll(): List<User> {
        return users
    }

    private val users: ArrayList<User> = arrayListOf<User>(testUser)

}