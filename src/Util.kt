package me.shigil

import io.ktor.application.ApplicationCall
import io.ktor.auth.authentication
import me.shigil.entity.User

val ApplicationCall.user get() = authentication.principal<User>()

val testUser = User(1, "Test", "+77009752083")